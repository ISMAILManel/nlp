### README pour le dossier de scraping et analyse des forums en français

## Structure du dossier `fr_forums`

Le dossier `fr_forums` contient divers fichiers liés à l'analyse des données des forums français. 

### Structure du dossier

```
fr_forums/
├── analysis_frenchdata.py
├── Average_sentiment_frenchdata.png
├── cigarette.png
├── forum_mamanpourlavie_scrapping.py
├── forum_posts1.csv
├── imc.png
├── post_french_preprocessed.csv
├── process_donneesfrancaises.py
├── repartition_generale.png
└── sport.png
```

### Description des scripts Python

1. **analysis_frenchdata.py**
   - **Objectif** : Ce script effectue l'analyse des données des forums français, y compris l'analyse de sentiment et la génération de visualisations.

   - **Fonctions principales** :
     - `load_data(file_path)`: Charge les données à partir d'un fichier CSV.
     - `analyze_sentiment(data)`: Effectue l'analyse de sentiment sur les données textuelles.
     - `generate_visualizations(data)`: Crée des graphiques et des visualisations basés sur l'analyse des données.

   - **Sorties** : Génère des fichiers image (PNG) montrant les résultats de l'analyse, tels que `Average_sentiment_frenchdata.png`, `repartition_generale.png`, etc.

2. **forum_mamanpourlavie_scrapping.py**
   - **Objectif** : Ce script extrait les données du forum "mamanpourlavie" en utilisant des techniques de web scraping.

   - **Fonctions principales** :
     - `scrape_forum(url)`: Extrait les messages et autres données pertinentes d'une page de forum donnée.
     - `parse_html(html)`: Analyse le code HTML de la page pour extraire les informations souhaitées.
     - `save_data(data, file_path)`: Enregistre les données extraites dans un fichier CSV.

   - **Sorties** : Produit un fichier CSV (`forum_posts1.csv`) contenant les données brutes des messages du forum.

3. **process_donneesfrancaises.py**

   - **Objectif** : Ce script traite les données brutes extraites des forums pour les préparer à l'analyse.

   - **Fonctions principales** :
     - `clean_data(data)`: Nettoie et normalise les données textuelles.
     - `preprocess_data(data)`: Effectue des transformations supplémentaires, comme la tokenisation et la suppression des stopwords.
     - `save_preprocessed_data(data, file_path)`: Enregistre les données prétraitées dans un fichier CSV.

   - **Sorties** : Produit un fichier CSV (`post_french_preprocessed.csv`) contenant les données des messages prétraitées, prêtes pour l'analyse.
