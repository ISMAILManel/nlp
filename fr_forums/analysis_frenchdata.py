import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Désactiver tous les messages de log de TensorFlow pour réduire le bruit dans la console

import pandas as pd
import spacy
from spacy.matcher import Matcher
from textblob import TextBlob
import matplotlib.pyplot as plt
import seaborn as sns
from multiprocessing import Pool
from wordcloud import WordCloud

# Charger spaCy avec uniquement les composants nécessaires pour économiser les ressources
nlp = spacy.load('fr_core_news_md', disable=['ner', 'parser'])

# Définir les termes à rechercher avec différentes variantes orthographiques
terms = {
    "alcool": ["alcool", "alcools", "alcoolique", "alcoolisme", "boire", "boit", "vin", "biere", "bière", "cocktail", "bouteille", "bar", "bois"],
    "cigarette": ["cigarette", "cigarettes", "cigare", "tabac", "fumer", "smoking", "fume"],
    "sport": ["sport", "sports", "sportive", "sportif", "activité physique", "exercice"],
    "IMC": ["IMC", "indice de masse corporelle", "masse corporelle", "poids", "gros", "grosse", "obèse", "obese", "obésité", "pd", "sucre", "sain", "légume", "légumes", "legume", "legumes"]
}

# Initialiser le Matcher de spaCy avec le vocabulaire chargé
matcher = Matcher(nlp.vocab)

# Ajouter des motifs au matcher pour chaque terme défini
for key, terms_list in terms.items():
    for term in terms_list:
        pattern = [{'LOWER': term}]
        matcher.add(key.upper(), [pattern])

# Fonction pour extraire les mots-clés et analyser le sentiment d'un texte
def extract_keywords_and_sentiment(text):
    if pd.isna(text) or not isinstance(text, str):
        return [], 0.0  # Retourner une liste vide et un score de sentiment neutre si le texte est manquant ou invalide
    doc = nlp(text.lower())  # Convertir le texte en minuscules et le traiter avec spaCy
    matches = matcher(doc)  # Trouver les motifs dans le texte
    keywords = list(set([doc[start:end].text for _, start, end in matches]))  # Extraire les mots-clés uniques
    sentiment = TextBlob(text).sentiment.polarity  # Analyser le sentiment avec TextBlob
    return keywords, sentiment  # Retourner les mots-clés et le score de sentiment

# Fonction pour traiter une ligne de données
def process_row(row):
    return extract_keywords_and_sentiment(row['Processed Comment'])

# Bloc principal pour s'assurer que le multiprocessing fonctionne correctement
if __name__ == "__main__":
    # Charger les données
    print("Chargement des données...")
    data = pd.read_csv('./post_french_preprocessed.csv')
    print("Données chargées.")

    # Filtrer les lignes avec des commentaires manquants
    data = data.dropna(subset=['Processed Comment'])

    # Afficher les premières lignes de données pour vérification
    print("Données initiales :")
    print(data.head())

    # Utiliser multiprocessing pour accélérer le traitement des données
    print("Extraction des mots-clés et analyse de sentiment...")
    with Pool(os.cpu_count()) as pool:
        results = pool.map(process_row, [row for _, row in data.iterrows()])

    # Ajouter les résultats au DataFrame
    data['keywords_sentiment'] = results
    print("Extraction terminée.")

    # Séparer les mots-clés et les sentiments dans des colonnes distinctes
    print("Séparation des mots-clés et des sentiments...")
    data['keywords'] = data['keywords_sentiment'].apply(lambda x: x[0])
    data['sentiment'] = data['keywords_sentiment'].apply(lambda x: x[1])
    data.drop(columns=['keywords_sentiment'], inplace=True)
    print("Séparation terminée.")

    # Fonction pour classifier les sentiments en positif, neutre ou négatif
    def classify_sentiment(polarity):
        if polarity > 0.1:
            return 'positif'
        elif polarity < -0.1:
            return 'négatif'
        else:
            return 'neutre'

    # Appliquer la classification des sentiments
    data['sentiment_label'] = data['sentiment'].apply(classify_sentiment)

    # Afficher les données après l'extraction des mots-clés, l'analyse de sentiment et la classification
    print("Données après extraction des mots-clés, analyse de sentiment et classification :")
    print(data[['Processed Comment', 'keywords', 'sentiment', 'sentiment_label']].head())

    # Exploser les mots-clés pour un groupement facile
    print("Explosion des mots-clés...")
    data_exploded = data.explode('keywords')
    print("Explosion terminée.")

    # Vérifier les données après l'explosion des mots-clés
    print("Données après explosion des mots-clés :")
    print(data_exploded.head())

    # Calculer les sentiments moyens par mots-clés
    print("Calcul des sentiments moyens par mots-clés...")
    keywords_sentiment = data_exploded.groupby('keywords')['sentiment'].mean().dropna()
    print("Calcul terminé.")

    # Afficher les résultats des sentiments moyens par mots-clés
    print("Sentiments moyens par mots-clés :")
    print(keywords_sentiment)

    # Visualiser les sentiments moyens par mots-clés
    print("Visualisation des résultats...")
    plt.figure(figsize=(10, 6))
    sns.barplot(x=keywords_sentiment.index, y=keywords_sentiment.values)
    plt.title('Average Sentiment by Health Keywords')
    plt.ylabel('Average Sentiment')
    plt.xlabel('Keywords')
    plt.xticks(rotation=45)
    plt.show()
    print("Visualisation terminée.")

    # Générer un nuage de mots à partir des mots-clés extraits
    print("Génération du nuage de mots...")
    all_keywords = data_exploded['keywords'].dropna().tolist()
    all_keywords_str = ' '.join(all_keywords)
    wordcloud = WordCloud(width=800, height=400, background_color='white').generate(all_keywords_str)

    # Afficher le nuage de mots
    plt.figure(figsize=(10, 6))
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis('off')
    plt.title('Word Cloud of Health Keywords')
    plt.show()
    print("Nuage de mots généré.")

    # Calculer la répartition générale des sentiments des posts et commentaires
    general_sentiment_counts = data['sentiment_label'].value_counts()
    print("Répartition générale des sentiments :")
    print(general_sentiment_counts)

    # Visualiser la répartition générale des sentiments en camembert
    plt.figure(figsize=(8, 8))
    plt.pie(general_sentiment_counts, labels=general_sentiment_counts.index, autopct='%1.1f%%', startangle=140)
    plt.title('Répartition générale des sentiments')
    plt.show()

    # Calculer et visualiser les sentiments par catégorie (alcool, cigarette, sport, IMC)
    categories = ['alcool', 'cigarette', 'sport', 'IMC']
    for category in categories:
        category_data = data_exploded.dropna(subset=['keywords'])
        category_data = category_data[category_data['keywords'].apply(lambda x: any(term in x for term in terms[category]))]
        category_sentiment_counts = category_data['sentiment_label'].value_counts()
        print(f"Répartition des sentiments pour la catégorie {category} :")
        print(category_sentiment_counts)

        # Visualiser les sentiments par catégorie en camembert
        plt.figure(figsize=(8, 8))
        plt.pie(category_sentiment_counts, labels=category_sentiment_counts.index, autopct='%1.1f%%', startangle=140)
        plt.title(f"Répartition des sentiments pour la catégorie {category}")
        plt.show()