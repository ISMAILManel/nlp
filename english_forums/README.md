
# README pour le dossier de scraping et analyse des forums en anglais

Ce dossier contient des sous-dossiers d'extraction et des scripts pour prétraiter, analyser et effectuer des analyses de sentiment sur les données de discussions des forums BabyCenter et Reddit.

# Structure du dossier

```
├── english_forums/
│   ├── data_analysis.py
│   ├── data_preprocess.py
│   ├── deep_learning.py
│   ├── sentiment_analysis.py
│   ├── preprocessed_baby_center.csv
│   ├── preprocessed_reddit.csv
│   ├── occurence_analysis_output/
│   ├── sentiment_analysis_output/
│   ├── logs/
│   ├── reddit/
│   ├── babycenter/
```

# Contenu du dossier

### **Sous-dossier pour le scraping du forum BabyCenter :**

Dossier contenant les scripts dédiés à l'extraction des discussions de BabyCenter et fichier CSV contenant les données extraites.

### **Sous-dossier pour le scraping du forum Reddit :**

Dossier contenant les scripts dédiés à l'extraction des discussions de Reddit et fichier CSV contenant les données extraites.

### **Scripts de traitement des données :**

## data_preprocess.py

- **Objectif** : Prétraiter les données extraites, y compris le nettoyage et la lemmatisation des commentaires.

- **Fonctions principale** :
    - `nltk_preprocess(comment)` : Fonction de nettoyage et de lemmatisation des commentaires en utilisant NLTK.

- **Sorties** :
    - `preprocessed_baby_center.csv` : Fichier CSV contenant les commentaires prétraités de BabyCenter.
    - `preprocessed_reddit.csv` : Fichier CSV contenant les commentaires prétraités de Reddit.

## data_analysis.py

- **Objectif** : Effectuer des analyses détaillées sur les données extraites, y compris l'analyse de co-occurrence, l'extraction de topics, et la génération de nuages de mots.

- **Fonctions principales** :
    - `further_clean_text(text)` : Nettoyage supplémentaire des commentaires en supprimant les mots non pertinents.
    - `generate_word_cloud(text, title, file_path)` : Génération de nuages de mots à partir du texte nettoyé.
    - `extract_topics(text_data, num_topics, num_words)` : Extraction des topics principaux des commentaires.
    - `plot_cooccurrence_matrix(text_data, forum_name, top_n_words)` : Création de matrices de co-occurrence pour visualiser les relations entre les mots les plus fréquents.
    - `plot_topics_wordclouds(topics, forum_name, num_topics)` : Visualisation des topics extraits sous forme de nuages de mots.

- **Sorties** :
    - Matrices de co-occurrence ajustées pour visualiser les relations entre les mots dans les discussions BabyCenter et Reddit.
    - Nuages de mots pour les principaux topics extraits des discussions BabyCenter.
    - Nuages de mots pour les principaux topics extraits des discussions Reddit.
    - Nuages de mots globaux pour visualiser les mots les plus fréquents dans les discussions des deux forums.

## sentiment_analysis.py

- **Objectif** : Effectuer une analyse de sentiment des discussions en utilisant TextBlob et VADER.

- **Fonctions principales** :
    - `nltk_preprocess(comment)` : Prétraitement des commentaires.
    - `textblob_sentiment_analysis(tokens)` : Analyse de sentiment avec TextBlob.
    - `vader_sentiment_analysis(tokens)` : Analyse de sentiment avec VADER.
    - `classify_sentiment(vader_score, textblob_score, tokens)` : Classification des sentiments en positif, négatif ou neutre.

- **Sorties** :
    - `sentiment_processed_baby_center_adjusted.csv` : Fichier CSV contenant les résultats de l'analyse de sentiment pour BabyCenter.
    - `sentiment_processed_reddit_adjusted.csv` : Fichier CSV contenant les résultats de l'analyse de sentiment pour Reddit.
    - Affichage des distributions de sentiments et exemples de commentaires classés.

## deep_learning.py

- **Objectif** : Utiliser des modèles de deep learning pour analyser les discussions, y compris la détection des sujets tels que l'alcool, le tabac, l'IMC et les problèmes de santé.

- **Fonctions principales** :
    - `contains_keywords(text, keywords)` : Vérifier si les commentaires contiennent des mots-clés spécifiques.
    - Création d'un modèle de deep learning avec Keras pour classer les commentaires selon les sujets d'intérêt.
    - Division les données en ensembles d'entraînement et de test.
    - Entraîner, évaluer et afficher les résultats du modèle.

- **Sorties** :
    - Affichage des scores de performance du modèle (précision, rappel, F1-score).
    - Matrice de confusion et exemples de prédictions sur les commentaires.

### **Sous-dossier pour les logs du deep learning :**

Ce dossier contient les journaux associés à l'exécution du script de deep learning.