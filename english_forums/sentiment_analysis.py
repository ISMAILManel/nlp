import pandas as pd
import nltk
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
import re
import string
from textblob import TextBlob
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from nltk.util import ngrams
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix

nltk.download('punkt')
nltk.download('wordnet')

lemmatizer = WordNetLemmatizer()
analyzer = SentimentIntensityAnalyzer()

# Définir des combinaisons spécifiques de tokens qui indiquent des sentiments négatifs et positifs
negative_combinations = {
    ('not', 'fine'): 'negative',
    ('not', 'healthy'): 'negative',
    ('fetal', 'alcohol'): 'negative',
    ('died',): 'negative',
    ('no', 'amount'): 'negative',
    ('not', 'okay'): 'negative',
    ('FAS',): 'negative',
    ('unsafe',): 'negative',
    ('risk',): 'negative',
    ('dangerous',): 'negative',
    ('harm',): 'negative',
    ('damage',): 'negative',
    ('fail', 'placenta'): 'negative',
    ('preterm',): 'negative',
    ('harmed',): 'negative',
    ('wrong',): 'negative',
    ('complication',): 'negative',
    ('miscarriage',): 'negative',
    ('weight',): 'negative',
    ('underweight',): 'negative',
    ('small',): 'negative',
    ('diabetes',): 'negative',
    ('smoking',): 'negative',
    ('obese',): 'negative',
    ('adhd',): 'negative',
    ('hyperactivity',): 'negative',
    ('deficient',): 'negative',
    ('disease',): 'negative',
    ('sick',): 'negative',
    ('permanent','consequences'): 'negative',
    ('learning','problems'): 'negative',
    ('said','no'): 'negative',
    ('uneasy'): 'negative',
    ('no','safe'): 'negative',
    ('health','issues'): 'negative',
    ('low','iq'): 'negative',
    ('emotional','problems'): 'negative',
    ('dysplasia',): 'negative',
    ('respirator',): 'negative',
    ('overweight',): 'negative',
    ('unhealthy',): 'negative',
    ('violent',): 'negative',
    ('obesity',): 'negative'
}

positive_combinations = {
    ('healthy',): 'positive',
    ('fine',): 'positive',
    ('safe',): 'positive',
    ('good',): 'positive',
    ('great',): 'positive',
    ('perfect',): 'positive',
    ('amazing',): 'positive',
    ('unharmed',): 'positive',
    ('is','okay'): 'positive',
    ('was','okay'): 'positive',
    ('were','okay'): 'positive',
    ('is','ok'): 'positive',
    ('ok'): 'positive',
    ('well', 'balanced'): 'positive',
    ('no', 'problems'): 'positive',
    ('no', 'issues'): 'positive',
    ('smart',): 'positive',
    ('happy',): 'positive',
    ('no','problem'): 'positive',
    ('less','worried'): 'positive',
    ('glad'): 'positive',
    ('successful'): 'positive',
    ('positive'): 'positive',
    ('well'): 'positive'
}

# Fonction de traitement des commentaires avec NLTK
def nltk_preprocess(comment):
    # Suppression des balises HTML
    comment = re.sub(r'<[^>]+>', '', comment)
    # Tokenisation du texte
    tokens = word_tokenize(comment)
    # Conversion en minuscules
    tokens = [token.lower() for token in tokens]
    # Suppression de la ponctuation
    table = str.maketrans('', '', string.punctuation)
    stripped = [token.translate(table) for token in tokens]
    # Lemmatisation
    lemmatized_words = [lemmatizer.lemmatize(word) for word in stripped if word]
    return lemmatized_words

# Fonction pour vérifier les combinaisons spécifiques de tokens
def check_token_combinations(tokens, combinations):
    for n in range(1, 3):  # Vérifie les unigrammes et les bigrammes
        for gram in ngrams(tokens, n):
            if gram in combinations:
                return True
    return False

# Fonction pour effectuer l'analyse de sentiments avec TextBlob
def textblob_sentiment_analysis(tokens):
    blob = TextBlob(' '.join(tokens))
    return blob.sentiment.polarity, blob.sentiment.subjectivity

# Fonction pour effectuer l'analyse de sentiments avec VADER
def vader_sentiment_analysis(tokens):
    vs = analyzer.polarity_scores(' '.join(tokens))
    return vs['compound'], vs['pos'], vs['neu'], vs['neg']

def classify_sentiment(vader_score, textblob_score, tokens):
    if check_token_combinations(tokens, negative_combinations):
        return 'negative'
    if check_token_combinations(tokens, positive_combinations):
        return 'positive'
    if vader_score >= 0.4 or textblob_score >= 0.4:
        return 'positive'
    elif vader_score <= -0.2 or textblob_score <= -0.2:
        return 'negative'
    else:
        return 'neutral'

def main():
    # Spécifiez les chemins des fichiers CSV
    baby_center_path = './babycenter/scrapped_data/baby_center_extraction_2024-05-29.csv'
    reddit_path = './reddit/web_scrapping/reddit/scrapping/data/extraction_2024-05-27.csv'

    # Chargement des fichiers CSV
    baby_center_df = pd.read_csv(baby_center_path)
    reddit_df = pd.read_csv(reddit_path)

    # Suppression des doublons
    baby_center_df.drop_duplicates(inplace=True)
    reddit_df.drop_duplicates(inplace=True)

    # Gestion des valeurs manquantes
    baby_center_df.fillna('', inplace=True)
    reddit_df.fillna('', inplace=True)

    # Appliquer le pré-traitement sur les colonnes de texte des DataFrames
    baby_center_df['Tokens'] = baby_center_df['Comment'].apply(nltk_preprocess)
    reddit_df['Tokens'] = reddit_df['Comment'].apply(nltk_preprocess)

    # Appliquer l'analyse de sentiments avec TextBlob et VADER
    baby_center_df['TextBlob_Polarity'], baby_center_df['TextBlob_Subjectivity'] = zip(*baby_center_df['Tokens'].apply(textblob_sentiment_analysis))
    reddit_df['TextBlob_Polarity'], reddit_df['TextBlob_Subjectivity'] = zip(*reddit_df['Tokens'].apply(textblob_sentiment_analysis))

    baby_center_df['VADER_Compound'], baby_center_df['VADER_Positive'], baby_center_df['VADER_Neutral'], baby_center_df['VADER_Negative'] = zip(*baby_center_df['Tokens'].apply(vader_sentiment_analysis))
    reddit_df['VADER_Compound'], reddit_df['VADER_Positive'], reddit_df['VADER_Neutral'], reddit_df['VADER_Negative'] = zip(*reddit_df['Tokens'].apply(vader_sentiment_analysis))

    # Classification des sentiments
    baby_center_df['Sentiment'] = baby_center_df.apply(lambda row: classify_sentiment(row['VADER_Compound'], row['TextBlob_Polarity'], row['Tokens']), axis=1)
    reddit_df['Sentiment'] = reddit_df.apply(lambda row: classify_sentiment(row['VADER_Compound'], row['TextBlob_Polarity'], row['Tokens']), axis=1)

    # Sauvegarder les données pré-traitées et analysées
    baby_center_df.to_csv('./sentiment_analysis_output/sentiment_processed_baby_center_adjusted.csv', index=False)
    reddit_df.to_csv('./sentiment_analysis_output/sentiment_processed_reddit_adjusted.csv', index=False)

    # Afficher les cinq premiers commentaires analysés pour vérifier
    print(baby_center_df[['Comment', 'Tokens', 'TextBlob_Polarity', 'TextBlob_Subjectivity', 'VADER_Compound', 'VADER_Positive', 'VADER_Neutral', 'VADER_Negative', 'Sentiment']].head())
    print(reddit_df[['Comment', 'Tokens', 'TextBlob_Polarity', 'TextBlob_Subjectivity', 'VADER_Compound', 'VADER_Positive', 'VADER_Neutral', 'VADER_Negative', 'Sentiment']].head())

    print("Sentiment analysis complete. Data saved to 'sentiment_processed_baby_center_adjusted.csv' and 'sentiment_processed_reddit_adjusted.csv'.")

    # Détails des résultats
    def sentiment_details(df, dataset_name):
        positive = df[df['Sentiment'] == 'positive'].shape[0]
        neutral = df[df['Sentiment'] == 'neutral'].shape[0]
        negative = df[df['Sentiment'] == 'negative'].shape[0]
        total = df.shape[0]
        print(f"\nSentiment distribution for {dataset_name}:")
        print(f"Positive: {positive} ({positive/total:.2%})")
        print(f"Neutral: {neutral} ({neutral/total:.2%})")
        print(f"Negative: {negative} ({negative/total:.2%})")
        print(f"Average TextBlob Polarity: {df['TextBlob_Polarity'].mean():.2f}")
        print(f"Average VADER Compound: {df['VADER_Compound'].mean():.2f}")

    sentiment_details(baby_center_df, 'Baby Center')
    sentiment_details(reddit_df, 'Reddit')

    # Afficher 5 commentaires positifs, neutres et négatifs
    print("\nExemples de commentaires positifs (Baby Center) :")
    print(baby_center_df[baby_center_df['Sentiment'] == 'positive']['Comment'].head(5).tolist())

    print("\nExemples de commentaires neutres (Baby Center) :")
    print(baby_center_df[baby_center_df['Sentiment'] == 'neutral']['Comment'].head(5).tolist())

    print("\nExemples de commentaires négatifs (Baby Center) :")
    print(baby_center_df[baby_center_df['Sentiment'] == 'negative']['Comment'].head(5).tolist())

    print("\nExemples de commentaires positifs (Reddit) :")
    print(reddit_df[reddit_df['Sentiment'] == 'positive']['Comment'].head(5).tolist())

    print("\nExemples de commentaires neutres (Reddit) :")
    print(reddit_df[reddit_df['Sentiment'] == 'neutral']['Comment'].head(5).tolist())

    print("\nExemples de commentaires négatifs (Reddit) :")
    print(reddit_df[reddit_df['Sentiment'] == 'negative']['Comment'].head(5).tolist())

    # Fonction pour créer un camembert de distribution des sentiments
    def plot_sentiment_distribution(df, dataset_name, colors):
        sentiment_counts = df['Sentiment'].value_counts(normalize=True)
        plt.figure(figsize=(8, 8))
        plt.pie(sentiment_counts, labels=sentiment_counts.index, autopct='%1.1f%%', startangle=140, colors=colors, textprops={'fontweight': 'bold'})
        plt.title(f'Distribution des sentiments pour {dataset_name}')
        plt.axis('equal')
        plt.savefig(f'./sentiment_analysis_output/sentiment_distribution_{dataset_name}.png')
        plt.show()

    # Fonction pour créer un histogramme des scores de polarité TextBlob
    def plot_polarity_histogram(df, dataset_name):
        plt.figure(figsize=(10, 6))
        plt.hist(df['TextBlob_Polarity'], bins=30, color='skyblue', edgecolor='black')
        plt.title(f'Histogramme des scores de polarité TextBlob pour {dataset_name}')
        plt.xlabel('Score de polarité')
        plt.ylabel('Fréquence')
        plt.savefig(f'./sentiment_analysis_output/polarity_histogram_{dataset_name}.png')
        plt.show()

    # Fonction pour créer un graphique en barres montrant la proportion de chaque sentiment pour chaque dataset
    def plot_proportion_bar_chart(df1, df2, label1, label2):
        sentiments = ['positive', 'neutral', 'negative']
        df1_proportions = df1['Sentiment'].value_counts(normalize=True)
        df2_proportions = df2['Sentiment'].value_counts(normalize=True)

        df1_proportions = df1_proportions.reindex(sentiments, fill_value=0)
        df2_proportions = df2_proportions.reindex(sentiments, fill_value=0)

        bar_width = 0.35
        index = range(len(sentiments))

        plt.figure(figsize=(10, 6))
        plt.bar(index, df1_proportions, bar_width, label=label1, color='#0072B2')
        plt.bar([i + bar_width for i in index], df2_proportions, bar_width, label=label2, color='#56B4E9')
        
        plt.xlabel('Sentiment')
        plt.ylabel('Proportion des commentaires')
        plt.title('Proportion des sentiments entre Baby Center et Reddit')
        plt.xticks([i + bar_width / 2 for i in index], sentiments)
        for i, v in enumerate(df1_proportions):
            plt.text(i - 0.1, v + 0.02, f"{v:.1%}", color='black', fontweight='bold')
        for i, v in enumerate(df2_proportions):
            plt.text(i + bar_width - 0.1, v + 0.02, f"{v:.1%}", color='black', fontweight='bold')
        plt.legend()
        plt.savefig('./sentiment_analysis_output/proportion_bar_chart.png')
        plt.show()

    # Fonction pour créer un radar chart des scores de polarité TextBlob et VADER
    def plot_radar_chart(df1, df2, label1, label2):
        labels = ['TextBlob Polarity', 'TextBlob Subjectivity', 'VADER Compound', 'VADER Positive', 'VADER Neutral', 'VADER Negative']
        stats1 = [
            df1['TextBlob_Polarity'].mean(),
            df1['TextBlob_Subjectivity'].mean(),
            df1['VADER_Compound'].mean(),
            df1['VADER_Positive'].mean(),
            df1['VADER_Neutral'].mean(),
            df1['VADER_Negative'].mean()
        ]
        stats2 = [
            df2['TextBlob_Polarity'].mean(),
            df2['TextBlob_Subjectivity'].mean(),
            df2['VADER_Compound'].mean(),
            df2['VADER_Positive'].mean(),
            df2['VADER_Neutral'].mean(),
            df2['VADER_Negative'].mean()
        ]

        angles = np.linspace(0, 2 * np.pi, len(labels), endpoint=False).tolist()
        stats1 += stats1[:1]
        stats2 += stats2[:1]
        angles += angles[:1]

        fig, ax = plt.subplots(figsize=(8, 8), subplot_kw=dict(polar=True))
        ax.fill(angles, stats1, color='#0072B2', alpha=0.25)
        ax.fill(angles, stats2, color='#56B4E9', alpha=0.25)
        ax.plot(angles, stats1, color='#0072B2', linewidth=2, label=label1)
        ax.plot(angles, stats2, color='#56B4E9', linewidth=2, label=label2)
        ax.set_yticklabels([])
        ax.set_xticks(angles[:-1])
        ax.set_xticklabels(labels)
        ax.legend(loc='upper right', bbox_to_anchor=(1.1, 1.1))
        plt.title('Comparaison des scores de sentiment entre Baby Center et Reddit')
        plt.savefig('./sentiment_analysis_output/radar_chart.png')
        plt.show()

    # Définir les couleurs pour le camembert
    colors = ['#0072B2', '#56B4E9', '#92C5DE']

    # Créer et sauvegarder les graphiques pour Baby Center
    plot_sentiment_distribution(baby_center_df, 'Baby Center', colors=colors)
    plot_polarity_histogram(baby_center_df, 'Baby Center')

    # Créer et sauvegarder les graphiques pour Reddit
    plot_sentiment_distribution(reddit_df, 'Reddit', colors=colors)
    plot_polarity_histogram(reddit_df, 'Reddit')

    # Créer et sauvegarder le graphique en barres des proportions
    plot_proportion_bar_chart(baby_center_df, reddit_df, 'Baby Center', 'Reddit')

    # Créer et sauvegarder le radar chart des scores de polarité et de sentiment
    plot_radar_chart(baby_center_df, reddit_df, 'Baby Center', 'Reddit')

if __name__ == "__main__":
    main()
