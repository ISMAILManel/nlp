# Readme projet NLP

**Programme :** M2 DSS

**Auteurs :**
- AANANOU Soukaina

- ISMAIL Manel

- MENZOU Katia


# Rendu du rapport final

Vous trouverez sur ce dépôt Git un fichier PDF correspondant à notre rapport final pour ce projet.

# Contenu du projet

Ce projet comprend des données extraites, prétraitées et analysées provenant de trois grandes sources d'information : 
- des articles scientifiques (sous-dossier : `/articles`)
- des forums français (sous-dossier : `/fr_forums`) 
- des forums anglais (sous-dossier : `/english_forums`).

