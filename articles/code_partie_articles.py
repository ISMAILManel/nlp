#!/usr/bin/env python
# coding: utf-8

# In[9]:


import re
import string
import nltk
from Bio import Entrez # Pour API 
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from transformers import BertTokenizer, BertModel
import torch
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.metrics import silhouette_score
from collections import defaultdict, Counter
from sklearn.feature_extraction.text import CountVectorizer
from wordcloud import WordCloud
import gensim
from gensim import corpora

# Télécharger les ressources nécessaires de nltk
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')

# Formuler la requête de recherche
query = "((pregnant women) AND (smoking OR alcohol OR obesity) AND (infant health OR baby health OR child health)) AND (future consequences OR long-term effects)"

# Fonction pour récupérer les IDs des articles
def fetch_article_ids(query, max_results=100): # nombre d'articles = 100
    handle = Entrez.esearch(db="pubmed", term=query, retmax=max_results)
    record = Entrez.read(handle)
    handle.close()
    return record["IdList"]

# Fonction pour récupérer les détails des articles
def fetch_article_details(ids):
    ids_str = ",".join(ids)
    handle = Entrez.efetch(db="pubmed", id=ids_str, retmode="xml")
    records = Entrez.read(handle)
    handle.close()
    return records

# Obtenir les IDs des articles correspondant à la requête
article_ids = fetch_article_ids(query)

# Obtenir les détails des articles
article_details = fetch_article_details(article_ids)


# In[33]:


import re
import string
import nltk
from Bio import Entrez
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from transformers import BertTokenizer, BertModel
import torch
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.metrics import silhouette_score
from collections import defaultdict, Counter
from sklearn.feature_extraction.text import CountVectorizer
from wordcloud import WordCloud
import gensim
from gensim import corpora

# Télécharger les ressources nécessaires de nltk
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')

# Liste de mots non pertinents supplémentaires dans un dictionnaire
additional_stopwords = {
    'pregnancy', 'women', 'pregnant', 'use', 'study', 'research', 'maternal', 
     'placenta',  'occasionally', '3rd', 'trimester', 'week', 'risk', 'pattern',
    'breast', 'feed','third' ,'expression', 'gestational'
    , 'healthy', 'author', 'medical', 'doctor',  'food', 'thing', 'placental', 'foetal', 
    'expression','exposure','year','association', 'associated','mother',
    'article', 'woman', 'infection', 'knowledg',
    'someone', 'permanent','management','week', 
     'possibility', 
    'instead', 'itervention', 'factor','aim','fetal','cohort', 'time', 'adverse','dietary',
    'among', 'including', 'taboo'}

# Fonction de prétraitement du texte
def preprocess_text(text):
    # Conversion du texte en minuscules
    text = text.lower()
    # Suppression de la ponctuation
    text = text.translate(str.maketrans('', '', string.punctuation))
    # Tokenization du texte
    words = word_tokenize(text)
    # Chargement des stopwords
    stop_words = set(stopwords.words('english'))
    # Union des stopwords personnalisés et des stopwords par défaut
    all_stopwords = stop_words.union( additional_stopwords)
    # Lemmatisation des mots
    lemmatizer = WordNetLemmatizer()
    words = [lemmatizer.lemmatize(word) for word in words if word not in all_stopwords]
    preprocessed_text = ' '.join(words)
    return preprocessed_text

# Prétraiter les textes des articles
preprocessed_texts = []

for article in article_details['PubmedArticle']:
    # Extraction du titre de l'article
    title = article['MedlineCitation']['Article']['ArticleTitle']
    # Extraction du résumé de l'article
    abstract = article['MedlineCitation']['Article']['Abstract']['AbstractText'][0] if 'Abstract' in article['MedlineCitation']['Article'] else 'No abstract available'
    # Combinaison du titre et du résumé
    full_text = f"{title} {abstract}"
    # Prétraitement du texte combiné
    preprocessed_text = preprocess_text(full_text)
    preprocessed_texts.append(preprocessed_text)
    print(preprocessed_texts)


# In[36]:


# Embedding 

#Embedding 1 avec BERT

from transformers import BertTokenizer, BertModel
import torch

# Charger le tokenizer et le modèle BERT
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
model = BertModel.from_pretrained('bert-base-uncased')

# Fonction pour générer les embeddings avec BERT
def get_bert_embeddings(texts):
    embeddings = []
    for text in texts:
        inputs = tokenizer(text, return_tensors='pt', truncation=True, padding=True, max_length=512)
        with torch.no_grad():
            outputs = model(**inputs)
        # Utiliser la sortie du token [CLS] comme embedding du texte
        cls_embedding = outputs.last_hidden_state[:, 0, :].squeeze()
        embeddings.append(cls_embedding)
    return embeddings

# Générer les embeddings pour les textes prétraités
embeddings = get_bert_embeddings(preprocessed_texts)

# Afficher les embeddings pour les premiers articles
for i, embedding in enumerate(embeddings[:5]):
    print(f"Embedding pour l'article {i+1}:\n{embedding}\n")


# In[ ]:


#Embedding 2 avec TF-IDF

from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd

# Créer le vectorizer TF-IDF
vectorizer = TfidfVectorizer()

# Transformer les snippets nettoyés en vecteurs TF-IDF
tfidf_matrix = vectorizer.fit_transform(preprocessed_texts)
tfidf_matrix

# Convertir la matrice TF-IDF en DataFrame pour visualisation
tfidf_df = pd.DataFrame(tfidf_matrix.toarray(), columns=vectorizer.get_feature_names_out())

# Visualiser les 5 premières lignes de la DataFrame TF-IDF
print(tfidf_df.head())

# Enregistrer la matrice TF-IDF dans un fichier CSV dans le répertoire spécifié
output_path = './output/tfidf_abstracts.csv'
tfidf_df.to_csv(output_path, index=False)


# In[32]:


# Fonction pour afficher la matrice de co-occurrence
def plot_cooccurrence_matrix(text_data, Articles, top_n_words=20):
    # Vectoriser le texte
    vectorizer = CountVectorizer(max_df=0.9, min_df=2, stop_words='english')
    dtm = vectorizer.fit_transform(text_data)
    words = vectorizer.get_feature_names_out()

    # Calculer la matrice de co-occurrence
    cooccurrence_matrix = (dtm.T * dtm)
    cooccurrence_matrix.setdiag(0)
    cooccurrence_matrix = cooccurrence_matrix.toarray()
    
    # Normaliser la matrice de co-occurrence
    cooccurrence_matrix_normalized = cooccurrence_matrix / cooccurrence_matrix.max()

    # Appliquer la transformation logarithmique
    cooccurrence_matrix_log = np.log1p(cooccurrence_matrix_normalized)

    # Obtenir les top N mots
    word_counts = np.array(dtm.sum(axis=0)).flatten()
    top_n_indices = word_counts.argsort()[-top_n_words:]
    top_words = [words[i] for i in top_n_indices]

    # Tracer la matrice de co-occurrence pour les top N mots
    top_cooccurrence_matrix = cooccurrence_matrix_log[top_n_indices][:, top_n_indices]

    plt.figure(figsize=(10, 10))
    plt.imshow(top_cooccurrence_matrix, cmap='viridis')
    plt.xticks(range(len(top_words)), top_words, rotation=90)
    plt.yticks(range(len(top_words)), top_words)
    plt.colorbar()
    plt.title(f'Matrice de Co-occurrence pour {Articles}')
    plt.show()

# Afficher la matrice de co-occurrence pour les textes prétraités
plot_cooccurrence_matrix(preprocessed_texts, 'Articles', top_n_words=20)


# In[38]:


from nltk.corpus import stopwords
from wordcloud import WordCloud
import matplotlib.pyplot as plt
import string
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation
import numpy as np
import gensim
from gensim import corpora

# Fonction pour générer un nuage de mots
def generate_word_cloud(text, title):
    wordcloud = WordCloud(width=800, height=400, background_color='white').generate(text)
    plt.figure(figsize=(10, 5))
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.title(title)
    plt.axis('off')
    plt.show()

# Générer le texte combiné pour le nuage de mots
combined_text = ' '.join(preprocessed_texts)

# Générer et afficher le nuage de mots
generate_word_cloud(combined_text, 'Word Cloud for Articles')

# Fonction pour extraire les topics
def extract_topics(texts, num_topics=5, num_words=10):
    # Prétraiter les textes
    texts = [text.split() for text in texts]
    
    # Créer le dictionnaire et le corpus
    dictionary = corpora.Dictionary(texts)
    corpus = [dictionary.doc2bow(text) for text in texts]
    
    # Appliquer le modèle LDA
    lda_model = gensim.models.ldamodel.LdaModel(corpus, num_topics=num_topics, id2word=dictionary, passes=15)
    
    # Extraire les topics
    topics = []
    for topic in lda_model.print_topics(num_words=num_words):
        topic_words = [word.split('*')[1].strip('"') for word in topic[1].split(' + ')]
        topics.append(topic_words)
    return topics

# Extraire les topics
topics = extract_topics(preprocessed_texts)

# Afficher les topics
print("Topics:")
for i, topic in enumerate(topics):
    print(f"Topic {i+1}: {', '.join(topic)}")

# Fonction pour visualiser les topics avec des nuages de mots
def plot_topics_wordclouds(topics, title_prefix):
    for i, topic in enumerate(topics):
        text = ' '.join(topic)
        generate_word_cloud(text, f'{title_prefix} Topic {i+1}')

# Visualiser les topics avec des nuages de mots
plot_topics_wordclouds(topics, 'Articles')


# In[ ]:




