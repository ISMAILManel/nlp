### README pour le dossier de scraping et analyse des articles scientifiques

## Structure du dossier `code_partie_articles`

Le dossier contient un script principal et un sous-dossier `output` pour stocker les résultats de l'analyse. 

### Structure du dossier

```
code_partie_articles/
├── code_partie_articles.py
└── output/
```

### Description du script python

#### `code_partie_articles.py`

- **Objectif** : Ce script effectue une analyse complète des articles scientifiques liés à la santé des enfants et des effets à long terme des habitudes des femmes enceintes (tabagisme, alcool, obésité).
  
- **Fonctions principales** :

  1. **fetch_article_ids(query, max_results=100)** : 
     - Formule la requête et récupère les IDs des articles pertinents à partir de la base de données PubMed.

     - **Sortie** : Liste des IDs d'articles.
  
  2. **fetch_article_details(ids)** :
     - Récupère les détails des articles en utilisant les IDs.

     - **Sortie** : Détails des articles au format XML.

  3. **preprocess_text(text)** :
     - Effectue le prétraitement du texte, y compris la conversion en minuscules, la suppression de la ponctuation, la tokenisation, l'élimination des stopwords et la lemmatisation.

     - **Sortie** : Texte prétraité.

  4. **get_bert_embeddings(texts)** :
     - Génère des embeddings pour les textes prétraités en utilisant le modèle BERT.

     - **Sortie** : Liste des embeddings.

  5. **plot_cooccurrence_matrix(text_data, Articles, top_n_words=20)** :
     - Affiche la matrice de co-occurrence des mots les plus fréquents dans les textes prétraités.

     - **Sortie** : Visualisation de la matrice de co-occurrence.

  6. **generate_word_cloud(text, title)** :
     - Génère un nuage de mots pour le texte combiné.

     - **Sortie** : Nuage de mots visualisé.

  7. **extract_topics(texts, num_topics=5, num_words=10)** :
     - Extrait les topics des textes prétraités en utilisant le modèle LDA (Latent Dirichlet Allocation).

     - **Sortie** : Liste des topics et des mots associés.

  8. **plot_topics_wordclouds(topics, title_prefix)** :
     - Visualise les topics avec des nuages de mots.

     - **Sortie** : Nuages de mots pour chaque topic.

- **Sorties** : 
  - `output/tfidf_abstracts.csv` : Contient la matrice TF-IDF des articles.
  - Visualisations générées dans le script (nuages de mots, matrices de co-occurrence, etc.).

